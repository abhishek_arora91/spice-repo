﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Spice.Data;
using Spice.Models;
using Spice.Models.ViewModels;
using Spice.Utility;

namespace Spice.Areas.Admin.Controllers
{
    [Authorize(Roles = SD.ManagerUser)]
    [Area("Admin")]
    public class SubCategoryController : Controller
    {
        private readonly ApplicationDbContext dbContext;
        [TempData]
        public string StatusMessage { get; set; }

        public SubCategoryController(ApplicationDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public async Task<IActionResult> Index()
        {
            var subCategories = await dbContext.SubCategory.Include(s => s.Category).ToListAsync();
            return View(subCategories);
        }

        public async Task<IActionResult> Create()
        {
            SubCategoryAndCategoryViewModel model = new SubCategoryAndCategoryViewModel()
            {
                CategoryList = await dbContext.Category.ToListAsync(),
                SubCategory = new Models.SubCategory(),
                SubCategoryList = await dbContext.SubCategory.OrderBy(sb => sb.Name).Select(sb => sb.Name).Distinct().ToListAsync()
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(SubCategoryAndCategoryViewModel model)
        {
            if (ModelState.IsValid)
            {
                var doesSubCategoryExists = dbContext.SubCategory.Include(s => s.Category).Where(s => s.Name == model.SubCategory.Name && s.CategoryId == model.SubCategory.CategoryId);
                if (doesSubCategoryExists.Count() > 0)
                {
                    //error
                    StatusMessage = "Error: Sub Category exists under " + doesSubCategoryExists.First().Category.Name + " category. Please use another name.";
                }
                else
                {
                    dbContext.SubCategory.Add(model.SubCategory);
                    await dbContext.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
            }
            SubCategoryAndCategoryViewModel modelVm = new SubCategoryAndCategoryViewModel()
            {
                CategoryList = await dbContext.Category.ToListAsync(),
                SubCategory = model.SubCategory,
                SubCategoryList = await dbContext.SubCategory.OrderBy(sb => sb.Name).Select(sb => sb.Name).Distinct().ToListAsync(),
                StatusMessage = StatusMessage
            };
            return View(modelVm);
        }

        [ActionName("GetSubCategory")]
        public async Task<IActionResult> GetSubCategory(int id)
        {
            List<SubCategory> subCategories = new List<SubCategory>();
            subCategories = await dbContext.SubCategory.Where(s => s.CategoryId == id).ToListAsync();
            return Json(new SelectList(subCategories, "Id", "Name"));
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var subCategory = await dbContext.SubCategory.FindAsync(id);
            if (subCategory == null)
            {
                return NotFound();
            }
            SubCategoryAndCategoryViewModel model = new SubCategoryAndCategoryViewModel()
            {
                CategoryList = await dbContext.Category.ToListAsync(),
                SubCategory = subCategory,
                SubCategoryList = await dbContext.SubCategory.OrderBy(sb => sb.Name).Select(sb => sb.Name).Distinct().ToListAsync()
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(SubCategoryAndCategoryViewModel model)
        {
            if (ModelState.IsValid)
            {
                var doesSubCategoryExists = dbContext.SubCategory.Include(s => s.Category).Where(s => s.Name == model.SubCategory.Name && s.CategoryId == model.SubCategory.CategoryId);
                if (doesSubCategoryExists.Count() > 0)
                {
                    //error
                    StatusMessage = "Error: Sub Category exists under " + doesSubCategoryExists.First().Category.Name + " category. Please use another name.";
                }
                else
                {
                    var subCatFromDB = await dbContext.SubCategory.FindAsync(model.SubCategory.Id);
                    subCatFromDB.Name = model.SubCategory.Name;
                    await dbContext.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
            }
            SubCategoryAndCategoryViewModel modelVm = new SubCategoryAndCategoryViewModel()
            {
                CategoryList = await dbContext.Category.ToListAsync(),
                SubCategory = model.SubCategory,
                SubCategoryList = await dbContext.SubCategory.OrderBy(sb => sb.Name).Select(sb => sb.Name).Distinct().ToListAsync(),
                StatusMessage = StatusMessage
            };
            return View(modelVm);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var subCategory = await dbContext.SubCategory.Include(s => s.Category).FirstAsync(sc => sc.Id == id);
            if (subCategory == null)
            {
                return NotFound();
            }
            return View(subCategory);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var subCategory = await dbContext.SubCategory.Include(s => s.Category).FirstAsync(sc => sc.Id == id);
            if (subCategory == null)
            {
                return NotFound();
            }
            return View(subCategory);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Delete")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var subCategory = await dbContext.SubCategory.FindAsync(id);
            if (subCategory == null)
            {
                return View();
            }
            dbContext.SubCategory.Remove(subCategory);
            await dbContext.SaveChangesAsync();
            return RedirectToAction("Index");
        }
    }
}