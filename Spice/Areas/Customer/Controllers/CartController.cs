﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Spice.Data;
using Spice.Models;
using Spice.Models.ViewModels;
using Spice.Utility;

namespace Spice.Areas.Customer.Controllers
{
    [Area("Customer")]
    public class CartController : Controller
    {
        private readonly ApplicationDbContext dbContext;

        [BindProperty]
        public OrderDetailsCart DetailsCart { get; set; }
        public CartController(ApplicationDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public async Task<IActionResult> Index()
        {
            DetailsCart = new OrderDetailsCart
            {
                OrderHeader = new OrderHeader()
            };
            DetailsCart.OrderHeader.OrderTotal = 0;

            var claimsIdentity = (ClaimsIdentity)User.Identity;
            var claim = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier);

            var cart = dbContext.ShoppingCart.Where(u => u.ApplicationUserId == claim.Value);
            if (cart != null)
            {
                DetailsCart.ListCart = cart.ToList();
            }
            foreach (var list in DetailsCart.ListCart)
            {
                list.MenuItem = await dbContext.MenuItem.FirstOrDefaultAsync(m => m.Id == list.MenuItemId);
                DetailsCart.OrderHeader.OrderTotal += list.Count * list.MenuItem.Price;
                //list.MenuItem.Description = HttpUtility.HtmlEncode(list.MenuItem.Description);
                if (list.MenuItem.Description.Length > 100)
                {
                    list.MenuItem.Description = list.MenuItem.Description.Substring(0, 99) + "....";
                }
            }
            DetailsCart.OrderHeader.OrderTotalOriginal = DetailsCart.OrderHeader.OrderTotal;

            if (HttpContext.Session.GetString(SD.ssCouponCode) != null)
            {
                DetailsCart.OrderHeader.CouponCode = HttpContext.Session.GetString(SD.ssCouponCode);
                var couponFromDb = await dbContext.Coupon.FirstOrDefaultAsync(c => c.Name.ToLower() == DetailsCart.OrderHeader.CouponCode.ToLower());
                DetailsCart.OrderHeader.OrderTotal = SD.DiscountedPrice(couponFromDb, DetailsCart.OrderHeader.OrderTotalOriginal);
            }
            return View(DetailsCart);
        }

        public IActionResult AddCoupon()
        {
            if (DetailsCart.OrderHeader.CouponCode == null)
            {
                DetailsCart.OrderHeader.CouponCode = string.Empty;
            }
            HttpContext.Session.SetString(SD.ssCouponCode, DetailsCart.OrderHeader.CouponCode);
            return RedirectToAction("Index");
        }

        public IActionResult RemoveCoupon()
        {
            HttpContext.Session.SetString(SD.ssCouponCode, string.Empty);
            return RedirectToAction("Index");
        }
    }
}